import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";

import {
  Container,
  Header,
  Content,
  Nav,
  Navbar,
  FlexboxGrid,
  Panel,
  Form,
  ButtonToolbar,
  Button,
  Footer,
} from "rsuite";
import "rsuite/dist/rsuite.min.css";
import { useDispatch, useSelector } from "react-redux";
import { addUser } from "../store/actions/sampleAction";

const signUp = () => {
  const router = useRouter();

  const [name, setName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [message, setMessage] = useState("");

  const dispatch = useDispatch();
  const sampleListData = useSelector((state) => state.sampleDataUser);
  const { users } = sampleListData;

  const handlerClickSignUp = async () => {
    // const loginPath = `${window.location.origin}/api/${email}?password=${password}`;
    const loginPath = `http://localhost:3000/createPassword/${password}`; ////get in backend

    if (email === "" || password === "")
      return setMessage("complete all fields of the form");

    var raw = JSON.stringify({
      name: name,
      lastName: lastName,
      email: email,
      password: password,
    });

    // var requestOptions: any = {
    //   method: "POST",
    //   body: raw,
    //   redirect: "follow",
    // };
    // // --------------------------------------------
    // get in backend
    var requestOptions = {
      method: "GET",
      redirect: "follow",
    };
    // --------------------------------------------

    const filterEmail = users.some((res) => res.email === email);
    if (filterEmail) {
      setMessage("The user already exists");
    } else {
      setMessage("");
      console.log("create User");
      fetch(loginPath, requestOptions)
        .then((response) => response.json())
        .then((result) => {
          const id =
            new Date().getTime().toString(36) + new Date().getUTCMilliseconds();
          localStorage.setItem("key", id);

          router.push("Home");
          dispatch(
            addUser({
              name: name,
              lastName: lastName,
              email: email,
              password: result.password,
              id: id,
            })
          );
        })
        .catch((error) => console.log("error", error));
    }
  };
  return (
    <div>
      <Container>
        <Header>
          <Navbar appearance="inverse" style={{ backgroundColor: "#3e47f5" }}>
            <Navbar.Brand>Technical Test</Navbar.Brand>
          </Navbar>
        </Header>

        <div style={{ textAlign: "center" }}>
          <h2>Sign Up</h2>
          <h4 style={{ color: "#3e47f5" }}>Create your account</h4>
        </div>

        <Content>
          <FlexboxGrid justify="center" style={{ margin: 20 }}>
            <FlexboxGrid.Item colspan={12}>
              <Panel header={<h3>Sign Up</h3>} bordered>
                <Form fluid>
                  <Form.Group>
                    <Form.ControlLabel>Name</Form.ControlLabel>
                    <Form.Control
                      name="Name"
                      type="text"
                      required
                      onChange={(e) => setName(e)}
                    />
                  </Form.Group>
                  <Form.Group>
                    <Form.ControlLabel>LastName</Form.ControlLabel>
                    <Form.Control
                      name="LastName"
                      type="text"
                      required
                      onChange={(e) => setLastName(e)}
                    />
                  </Form.Group>
                  <Form.Group>
                    <Form.ControlLabel>Email</Form.ControlLabel>
                    <Form.Control
                      name="email"
                      type="email"
                      onChange={(e) => setEmail(e)}
                    />
                  </Form.Group>
                  <Form.Group>
                    <Form.ControlLabel>Password</Form.ControlLabel>
                    <Form.Control
                      name="password"
                      type="password"
                      onChange={(e) => setPassword(e)}
                    />
                  </Form.Group>
                  <Form.Group>
                    <ButtonToolbar>
                      <Button
                        appearance="primary"
                        color="green"
                        onClick={handlerClickSignUp}
                      >
                        Sign Up
                      </Button>
                      <Button
                        appearance="link"
                        onClick={() => router.push("/")}
                      >
                        Login
                      </Button>
                    </ButtonToolbar>
                  </Form.Group>
                </Form>
                <p style={{ color: "red" }}>{message}</p>
              </Panel>
            </FlexboxGrid.Item>
          </FlexboxGrid>
        </Content>
        <Footer style={{ textAlign: "center", margin: 20 }}>
          © Dalia Diaz
        </Footer>
      </Container>
    </div>
  );
};

export default signUp;
