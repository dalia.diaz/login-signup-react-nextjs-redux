import React, { useState, useEffect } from "react";
import {
  Container,
  Header,
  Content,
  Nav,
  Navbar,
  FlexboxGrid,
  Panel,
  Footer,
  Input,
  Avatar,
} from "rsuite";
import { useRouter } from "next/router";
import { useSelector } from "react-redux";

import "rsuite/dist/rsuite.min.css";
const Home = () => {
  const router = useRouter();
  const sampleListData = useSelector((state) => state.sampleDataUser);
  const { users } = sampleListData;
  const [data, setData] = useState<any>({});

  const logOut = () => {
    router.push("/");
    localStorage.removeItem("key");
  };

  useEffect(() => {
    let id = localStorage.getItem("key");
    const getData = users.find((res) => res.id === id);

    if (getData) setData(getData);
    else {
      logOut();
    }
  }, []);

  return (
    <div>
      <Container>
        <Header>
          <Navbar appearance="inverse" style={{ backgroundColor: "#3e47f5" }}>
            <Navbar.Brand>Technical Test</Navbar.Brand>
            <Nav>
              <Nav.Item>User Information</Nav.Item>
            </Nav>
            <Nav pullRight>
              <Nav.Menu title="Log Out">
                <Nav.Item onClick={logOut}>log out </Nav.Item>
              </Nav.Menu>
            </Nav>
          </Navbar>
        </Header>

        <div style={{ textAlign: "center" }}>
          <h2>Profile</h2>
        </div>

        <Content>
          <FlexboxGrid justify="center" style={{ margin: 20 }}>
            <FlexboxGrid.Item colspan={12}>
              <Panel header={<h3>User Information</h3>} bordered>
                <label htmlFor="">Id</label>
                <Input value={data?.id ?? ""} disabled={true} />
                <label htmlFor="">Name</label>
                <Input value={data?.name ?? ""} disabled={true} />
                <label htmlFor="">LastName</label>
                <Input value={data?.lastName ?? ""} disabled={true} />
                <label htmlFor="">E-mail</label>
                <Input value={data?.email ?? ""} disabled={true} />
                <div style={{ textAlign: "center" }}>
                  <Avatar
                    style={{ top: "10px" }}
                    circle
                    size="lg"
                    src="https://w7.pngwing.com/pngs/178/595/png-transparent-user-profile-computer-icons-login-user-avatars-thumbnail.png"
                  />
                </div>
              </Panel>
            </FlexboxGrid.Item>
          </FlexboxGrid>
        </Content>
        <Footer style={{ textAlign: "center", margin: 20 }}>
          © Dalia Diaz
        </Footer>
      </Container>
    </div>
  );
};

export default Home;
