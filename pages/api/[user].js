import bcrypt from "bcrypt";

const handler = async (req, res) => {
  const method = req.method;
  switch (method) {
    case "GET":
      const user = req.query.user;

      const passwordMatch = await bcrypt.compare(
        req.query.password,
        req.query.match
      );

      res.json({ status: true, password: passwordMatch });

      break;
    case "POST":
      const jsonData = JSON.parse(req.body);
      const { name, lastname, email, password } = jsonData;

      const passwordHashed = await bcrypt.hash(password, 10);

      res.json({ status: true, message: "Success", password: passwordHashed });

      break;
  }
};

export default handler;
