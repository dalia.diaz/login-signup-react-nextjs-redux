import React, { useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";

import {
  Container,
  Header,
  Content,
  Navbar,
  FlexboxGrid,
  Panel,
  Form,
  ButtonToolbar,
  Button,
  Footer,
  Avatar,
} from "rsuite";
import "rsuite/dist/rsuite.min.css";
import { useSelector } from "react-redux";

export default function App() {
  const router = useRouter();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [message, setMessage] = useState("");

  const sampleListData = useSelector((state) => state.sampleDataUser);
  const { users } = sampleListData;

  const handlerClickLogin = async () => {
    var requestOptions: any = {
      method: "GET",
      redirect: "follow",
    };
    if (email === "" || password === "")
      return setMessage("complete all fields of the form");

    const filterEmail = users.some((res) => res.email === email);

    if (filterEmail) {
      const getPassword = users.find((res) => res.email === email);
      // const loginPath = `${window.location.origin}/api/${email}?password=${password}&match=${getPassword.password}`;
      const loginPath = `http://localhost:3000/getPassword/${password}/${getPassword.password}`;  //get in backend
      fetch(loginPath, requestOptions)
        .then((response) => response.json())
        .then((result) => {
          console.log(result.password);
          
          if (result.password) {
            localStorage.setItem("key", getPassword.id);
            setMessage("");

            router.push("Home");
          } else {
            setMessage("the Password or email is incorrect");
          }
        })
        .catch((error) => console.log("error", error));
    } else {
      console.log(filterEmail, "filterEmail");
      setMessage("the user is not registered");
    }
  };

  return (
    <div>
      <Container>
        <Header>
          <Navbar appearance="inverse" style={{ backgroundColor: "#3e47f5" }}>
            <Navbar.Brand>Technical Test</Navbar.Brand>
          </Navbar>
        </Header>

        <div style={{ textAlign: "center" }}>
          <h2>Login</h2>
          <Avatar
            style={{ top: "10px" }}
            circle
            size="lg"
            src="https://media.licdn.com/dms/image/C4E0BAQGjPZrUoNiG-Q/company-logo_200_200/0/1519903101345?e=2147483647&v=beta&t=ML50NHIU2NOkWYipKlE7fJ-N8ZrqIu3O62PWxTwKCog"
          />
        </div>

        <Content>
          <FlexboxGrid justify="center" style={{ margin: 20 }}>
            <FlexboxGrid.Item colspan={12}>
              <Panel header={<h3>Login</h3>} bordered>
                <Form fluid>
                  <Form.Group>
                    <Form.ControlLabel>Email</Form.ControlLabel>
                    <Form.Control
                      name="email"
                      type="email"
                      required
                      onChange={(e) => setEmail(e)}
                    />
                  </Form.Group>
                  <Form.Group>
                    <Form.ControlLabel>Password</Form.ControlLabel>
                    <Form.Control
                      name="password"
                      type="password"
                      onChange={(e) => setPassword(e)}
                    />
                  </Form.Group>
                  <Form.Group>
                    <ButtonToolbar>
                      <Button
                        appearance="primary"
                        color="green"
                        onClick={handlerClickLogin}
                      >
                        Sign in
                      </Button>
                      <Link href="/signUp" as="/signUp">
                        <Button appearance="link">Sign Up</Button>
                      </Link>
                    </ButtonToolbar>
                  </Form.Group>
                </Form>
                <p style={{ color: "red" }}>{message}</p>
              </Panel>
            </FlexboxGrid.Item>
          </FlexboxGrid>
        </Content>
        <Footer style={{ textAlign: "center", margin: 20 }}>
          © Dalia Diaz
        </Footer>
      </Container>
    </div>
  );
}
