"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/api/[user]";
exports.ids = ["pages/api/[user]"];
exports.modules = {

/***/ "bcrypt":
/*!*************************!*\
  !*** external "bcrypt" ***!
  \*************************/
/***/ ((module) => {

module.exports = require("bcrypt");

/***/ }),

/***/ "(api)/./pages/api/[user].js":
/*!*****************************!*\
  !*** ./pages/api/[user].js ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var bcrypt__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! bcrypt */ \"bcrypt\");\n/* harmony import */ var bcrypt__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(bcrypt__WEBPACK_IMPORTED_MODULE_0__);\n\nconst handler = async (req, res)=>{\n    const method = req.method;\n    switch(method){\n        case \"GET\":\n            const user = req.query.user;\n            const passwordMatch = await bcrypt__WEBPACK_IMPORTED_MODULE_0___default().compare(req.query.password, req.query.match);\n            res.json({\n                status: true,\n                password: passwordMatch\n            });\n            break;\n        case \"POST\":\n            const jsonData = JSON.parse(req.body);\n            const { name , lastname , email , password  } = jsonData;\n            const passwordHashed = await bcrypt__WEBPACK_IMPORTED_MODULE_0___default().hash(password, 10);\n            res.json({\n                status: true,\n                message: \"Success\",\n                password: passwordHashed\n            });\n            break;\n    }\n};\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (handler);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiKGFwaSkvLi9wYWdlcy9hcGkvW3VzZXJdLmpzLmpzIiwibWFwcGluZ3MiOiI7Ozs7OztBQUE0QjtBQUU1QixNQUFNQyxVQUFVLE9BQU9DLEtBQUtDLE1BQVE7SUFDbEMsTUFBTUMsU0FBU0YsSUFBSUUsTUFBTTtJQUN6QixPQUFRQTtRQUNOLEtBQUs7WUFDSCxNQUFNQyxPQUFPSCxJQUFJSSxLQUFLLENBQUNELElBQUk7WUFFM0IsTUFBTUUsZ0JBQWdCLE1BQU1QLHFEQUFjLENBQ3hDRSxJQUFJSSxLQUFLLENBQUNHLFFBQVEsRUFDbEJQLElBQUlJLEtBQUssQ0FBQ0ksS0FBSztZQUdqQlAsSUFBSVEsSUFBSSxDQUFDO2dCQUFFQyxRQUFRLElBQUk7Z0JBQUVILFVBQVVGO1lBQWM7WUFFakQsS0FBTTtRQUNSLEtBQUs7WUFDSCxNQUFNTSxXQUFXQyxLQUFLQyxLQUFLLENBQUNiLElBQUljLElBQUk7WUFDcEMsTUFBTSxFQUFFQyxLQUFJLEVBQUVDLFNBQVEsRUFBRUMsTUFBSyxFQUFFVixTQUFRLEVBQUUsR0FBR0k7WUFFNUMsTUFBTU8saUJBQWlCLE1BQU1wQixrREFBVyxDQUFDUyxVQUFVO1lBRW5ETixJQUFJUSxJQUFJLENBQUM7Z0JBQUVDLFFBQVEsSUFBSTtnQkFBRVUsU0FBUztnQkFBV2IsVUFBVVc7WUFBZTtZQUV0RSxLQUFNO0lBQ1Y7QUFDRjtBQUVBLGlFQUFlbkIsT0FBT0EsRUFBQyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3BhZ2VzL2FwaS9bdXNlcl0uanM/MzRlOCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgYmNyeXB0IGZyb20gXCJiY3J5cHRcIjtcblxuY29uc3QgaGFuZGxlciA9IGFzeW5jIChyZXEsIHJlcykgPT4ge1xuICBjb25zdCBtZXRob2QgPSByZXEubWV0aG9kO1xuICBzd2l0Y2ggKG1ldGhvZCkge1xuICAgIGNhc2UgXCJHRVRcIjpcbiAgICAgIGNvbnN0IHVzZXIgPSByZXEucXVlcnkudXNlcjtcblxuICAgICAgY29uc3QgcGFzc3dvcmRNYXRjaCA9IGF3YWl0IGJjcnlwdC5jb21wYXJlKFxuICAgICAgICByZXEucXVlcnkucGFzc3dvcmQsXG4gICAgICAgIHJlcS5xdWVyeS5tYXRjaFxuICAgICAgKTtcblxuICAgICAgcmVzLmpzb24oeyBzdGF0dXM6IHRydWUsIHBhc3N3b3JkOiBwYXNzd29yZE1hdGNoIH0pO1xuXG4gICAgICBicmVhaztcbiAgICBjYXNlIFwiUE9TVFwiOlxuICAgICAgY29uc3QganNvbkRhdGEgPSBKU09OLnBhcnNlKHJlcS5ib2R5KTtcbiAgICAgIGNvbnN0IHsgbmFtZSwgbGFzdG5hbWUsIGVtYWlsLCBwYXNzd29yZCB9ID0ganNvbkRhdGE7XG5cbiAgICAgIGNvbnN0IHBhc3N3b3JkSGFzaGVkID0gYXdhaXQgYmNyeXB0Lmhhc2gocGFzc3dvcmQsIDEwKTtcblxuICAgICAgcmVzLmpzb24oeyBzdGF0dXM6IHRydWUsIG1lc3NhZ2U6IFwiU3VjY2Vzc1wiLCBwYXNzd29yZDogcGFzc3dvcmRIYXNoZWQgfSk7XG5cbiAgICAgIGJyZWFrO1xuICB9XG59O1xuXG5leHBvcnQgZGVmYXVsdCBoYW5kbGVyO1xuIl0sIm5hbWVzIjpbImJjcnlwdCIsImhhbmRsZXIiLCJyZXEiLCJyZXMiLCJtZXRob2QiLCJ1c2VyIiwicXVlcnkiLCJwYXNzd29yZE1hdGNoIiwiY29tcGFyZSIsInBhc3N3b3JkIiwibWF0Y2giLCJqc29uIiwic3RhdHVzIiwianNvbkRhdGEiLCJKU09OIiwicGFyc2UiLCJib2R5IiwibmFtZSIsImxhc3RuYW1lIiwiZW1haWwiLCJwYXNzd29yZEhhc2hlZCIsImhhc2giLCJtZXNzYWdlIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///(api)/./pages/api/[user].js\n");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("(api)/./pages/api/[user].js"));
module.exports = __webpack_exports__;

})();