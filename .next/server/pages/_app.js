"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/_app";
exports.ids = ["pages/_app"];
exports.modules = {

/***/ "./pages/_app.js":
/*!***********************!*\
  !*** ./pages/_app.js ***!
  \***********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _store_store__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../store/store */ \"./store/store.js\");\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ \"react-redux\");\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_2__);\n\n\n\nfunction MyApp({ Component , pageProps  }) {\n    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {\n        children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_redux__WEBPACK_IMPORTED_MODULE_2__.Provider, {\n            store: _store_store__WEBPACK_IMPORTED_MODULE_1__.store,\n            children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(Component, {\n                ...pageProps\n            }, void 0, false, {\n                fileName: \"/home/dalia/Desktop/Valley Tech/login-signup-react-nextjs-redux/pages/_app.js\",\n                lineNumber: 8,\n                columnNumber: 9\n            }, this)\n        }, void 0, false, {\n            fileName: \"/home/dalia/Desktop/Valley Tech/login-signup-react-nextjs-redux/pages/_app.js\",\n            lineNumber: 7,\n            columnNumber: 7\n        }, this)\n    }, void 0, false);\n}\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_store_store__WEBPACK_IMPORTED_MODULE_1__.wrapper.withRedux(MyApp));\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9fYXBwLmpzLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBZ0Q7QUFDVDtBQUV2QyxTQUFTRyxNQUFNLEVBQUVDLFVBQVMsRUFBRUMsVUFBUyxFQUFFLEVBQUU7SUFDdkMscUJBQ0U7a0JBQ0UsNEVBQUNILGlEQUFRQTtZQUFDRCxPQUFPQSwrQ0FBS0E7c0JBQ3BCLDRFQUFDRztnQkFBVyxHQUFHQyxTQUFTOzs7Ozs7Ozs7Ozs7QUFJaEM7QUFFQSxpRUFBZUwsMkRBQWlCLENBQUNHLE1BQU1BLEVBQUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9wYWdlcy9fYXBwLmpzP2UwYWQiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgd3JhcHBlciwgc3RvcmUgfSBmcm9tIFwiLi4vc3RvcmUvc3RvcmVcIjtcbmltcG9ydCB7IFByb3ZpZGVyIH0gZnJvbSBcInJlYWN0LXJlZHV4XCI7XG5cbmZ1bmN0aW9uIE15QXBwKHsgQ29tcG9uZW50LCBwYWdlUHJvcHMgfSkge1xuICByZXR1cm4gKFxuICAgIDw+XG4gICAgICA8UHJvdmlkZXIgc3RvcmU9e3N0b3JlfT5cbiAgICAgICAgPENvbXBvbmVudCB7Li4ucGFnZVByb3BzfSAvPlxuICAgICAgPC9Qcm92aWRlcj5cbiAgICA8Lz5cbiAgKTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgd3JhcHBlci53aXRoUmVkdXgoTXlBcHApO1xuIl0sIm5hbWVzIjpbIndyYXBwZXIiLCJzdG9yZSIsIlByb3ZpZGVyIiwiTXlBcHAiLCJDb21wb25lbnQiLCJwYWdlUHJvcHMiLCJ3aXRoUmVkdXgiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./pages/_app.js\n");

/***/ }),

/***/ "./store/reducers/index.js":
/*!*********************************!*\
  !*** ./store/reducers/index.js ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ \"redux\");\n/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _sampleReducer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./sampleReducer */ \"./store/reducers/sampleReducer.js\");\n\n\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((0,redux__WEBPACK_IMPORTED_MODULE_0__.combineReducers)({\n    sampleDataUser: _sampleReducer__WEBPACK_IMPORTED_MODULE_1__[\"default\"]\n}));\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zdG9yZS9yZWR1Y2Vycy9pbmRleC5qcy5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQXdDO0FBQ0k7QUFFNUMsaUVBQWVBLHNEQUFlQSxDQUFDO0lBQzdCRSxnQkFBZ0JELHNEQUFhQTtBQUMvQixFQUFFLEVBQUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zdG9yZS9yZWR1Y2Vycy9pbmRleC5qcz9iMjQyIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGNvbWJpbmVSZWR1Y2VycyB9IGZyb20gXCJyZWR1eFwiO1xuaW1wb3J0IHNhbXBsZVJlZHVjZXIgZnJvbSBcIi4vc2FtcGxlUmVkdWNlclwiO1xuXG5leHBvcnQgZGVmYXVsdCBjb21iaW5lUmVkdWNlcnMoe1xuICBzYW1wbGVEYXRhVXNlcjogc2FtcGxlUmVkdWNlcixcbn0pO1xuIl0sIm5hbWVzIjpbImNvbWJpbmVSZWR1Y2VycyIsInNhbXBsZVJlZHVjZXIiLCJzYW1wbGVEYXRhVXNlciJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./store/reducers/index.js\n");

/***/ }),

/***/ "./store/reducers/sampleReducer.js":
/*!*****************************************!*\
  !*** ./store/reducers/sampleReducer.js ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\nconst initialState = {\n    sample: [],\n    loading: true,\n    users: [\n        {\n            email: \"admin@admin.com\",\n            id: \"$2b$10$rVwJGVobfIUHRI4rP7zN0OmT/fUJoSbUYR8acdtsrcbWTheXWTi5i\",\n            lastName: \"admin\",\n            name: \"admin\",\n            password: \"$2b$10$iB.YY0oJywHrpRqonTkrBOp3ISgRsur5rhB0a66v42LffAkkL7c.6\"\n        }\n    ]\n};\nconst sampleReducer = (state = initialState, action)=>{\n    switch(action.type){\n        case \"ADD_USER\":\n            console.log(\"ADD_USER\", action.payload, state.users);\n            const updatedData = state.users.concat(action.payload.users);\n            return {\n                ...state,\n                loading: false,\n                users: updatedData\n            };\n        default:\n            return state;\n    }\n};\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (sampleReducer);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zdG9yZS9yZWR1Y2Vycy9zYW1wbGVSZWR1Y2VyLmpzLmpzIiwibWFwcGluZ3MiOiI7Ozs7QUFDQSxNQUFNQSxlQUFlO0lBQ25CQyxRQUFRLEVBQUU7SUFDVkMsU0FBUyxJQUFJO0lBQ2JDLE9BQU87UUFDTDtZQUNFQyxPQUFPO1lBQ1BDLElBQUk7WUFDSkMsVUFBVTtZQUNWQyxNQUFNO1lBQ05DLFVBQVU7UUFDWjtLQUNEO0FBQ0g7QUFFQSxNQUFNQyxnQkFBZ0IsQ0FBQ0MsUUFBUVYsWUFBWSxFQUFFVyxTQUFXO0lBQ3RELE9BQVFBLE9BQU9DLElBQUk7UUFDakIsS0FBSztZQUNIQyxRQUFRQyxHQUFHLENBQUMsWUFBWUgsT0FBT0ksT0FBTyxFQUFFTCxNQUFNUCxLQUFLO1lBQ25ELE1BQU1hLGNBQWNOLE1BQU1QLEtBQUssQ0FBQ2MsTUFBTSxDQUFDTixPQUFPSSxPQUFPLENBQUNaLEtBQUs7WUFFM0QsT0FBTztnQkFDTCxHQUFHTyxLQUFLO2dCQUNSUixTQUFTLEtBQUs7Z0JBQ2RDLE9BQU9hO1lBQ1Q7UUFFRjtZQUNFLE9BQU9OO0lBQ1g7QUFDRjtBQUVBLGlFQUFlRCxhQUFhQSxFQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vc3RvcmUvcmVkdWNlcnMvc2FtcGxlUmVkdWNlci5qcz9jZDRkIl0sInNvdXJjZXNDb250ZW50IjpbIlxuY29uc3QgaW5pdGlhbFN0YXRlID0ge1xuICBzYW1wbGU6IFtdLFxuICBsb2FkaW5nOiB0cnVlLFxuICB1c2VyczogW1xuICAgIHtcbiAgICAgIGVtYWlsOiBcImFkbWluQGFkbWluLmNvbVwiLFxuICAgICAgaWQ6IFwiJDJiJDEwJHJWd0pHVm9iZklVSFJJNHJQN3pOME9tVC9mVUpvU2JVWVI4YWNkdHNyY2JXVGhlWFdUaTVpXCIsXG4gICAgICBsYXN0TmFtZTogXCJhZG1pblwiLFxuICAgICAgbmFtZTogXCJhZG1pblwiLFxuICAgICAgcGFzc3dvcmQ6IFwiJDJiJDEwJGlCLllZMG9KeXdIcnBScW9uVGtyQk9wM0lTZ1JzdXI1cmhCMGE2NnY0MkxmZkFra0w3Yy42XCIsXG4gICAgfSxcbiAgXSxcbn07XG5cbmNvbnN0IHNhbXBsZVJlZHVjZXIgPSAoc3RhdGUgPSBpbml0aWFsU3RhdGUsIGFjdGlvbikgPT4ge1xuICBzd2l0Y2ggKGFjdGlvbi50eXBlKSB7XG4gICAgY2FzZSBcIkFERF9VU0VSXCI6XG4gICAgICBjb25zb2xlLmxvZyhcIkFERF9VU0VSXCIsIGFjdGlvbi5wYXlsb2FkLCBzdGF0ZS51c2Vycyk7XG4gICAgICBjb25zdCB1cGRhdGVkRGF0YSA9IHN0YXRlLnVzZXJzLmNvbmNhdChhY3Rpb24ucGF5bG9hZC51c2Vycyk7XG5cbiAgICAgIHJldHVybiB7XG4gICAgICAgIC4uLnN0YXRlLFxuICAgICAgICBsb2FkaW5nOiBmYWxzZSxcbiAgICAgICAgdXNlcnM6IHVwZGF0ZWREYXRhLFxuICAgICAgfTtcblxuICAgIGRlZmF1bHQ6XG4gICAgICByZXR1cm4gc3RhdGU7XG4gIH1cbn07XG5cbmV4cG9ydCBkZWZhdWx0IHNhbXBsZVJlZHVjZXI7XG4iXSwibmFtZXMiOlsiaW5pdGlhbFN0YXRlIiwic2FtcGxlIiwibG9hZGluZyIsInVzZXJzIiwiZW1haWwiLCJpZCIsImxhc3ROYW1lIiwibmFtZSIsInBhc3N3b3JkIiwic2FtcGxlUmVkdWNlciIsInN0YXRlIiwiYWN0aW9uIiwidHlwZSIsImNvbnNvbGUiLCJsb2ciLCJwYXlsb2FkIiwidXBkYXRlZERhdGEiLCJjb25jYXQiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./store/reducers/sampleReducer.js\n");

/***/ }),

/***/ "./store/store.js":
/*!************************!*\
  !*** ./store/store.js ***!
  \************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"store\": () => (/* binding */ store),\n/* harmony export */   \"wrapper\": () => (/* binding */ wrapper)\n/* harmony export */ });\n/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ \"redux\");\n/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var redux_thunk__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-thunk */ \"redux-thunk\");\n/* harmony import */ var redux_thunk__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_thunk__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var redux_devtools_extension__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! redux-devtools-extension */ \"redux-devtools-extension\");\n/* harmony import */ var redux_devtools_extension__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(redux_devtools_extension__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var next_redux_wrapper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next-redux-wrapper */ \"next-redux-wrapper\");\n/* harmony import */ var next_redux_wrapper__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_redux_wrapper__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var _reducers__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./reducers */ \"./store/reducers/index.js\");\n\n\n\n\n\n// initial states here\nconst initalState = {};\n// middleware\nconst middleware = [\n    (redux_thunk__WEBPACK_IMPORTED_MODULE_1___default())\n];\n// creating store\nconst store = (0,redux__WEBPACK_IMPORTED_MODULE_0__.createStore)(_reducers__WEBPACK_IMPORTED_MODULE_4__[\"default\"], initalState, (0,redux_devtools_extension__WEBPACK_IMPORTED_MODULE_2__.composeWithDevTools)((0,redux__WEBPACK_IMPORTED_MODULE_0__.applyMiddleware)(...middleware)));\n// assigning store to next wrapper\nconst makeStore = ()=>store;\nconst wrapper = (0,next_redux_wrapper__WEBPACK_IMPORTED_MODULE_3__.createWrapper)(makeStore);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zdG9yZS9zdG9yZS5qcy5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztBQUFxRDtBQUNyQjtBQUMrQjtBQUNaO0FBQ2Q7QUFFckMsc0JBQXNCO0FBQ3RCLE1BQU1NLGNBQWMsQ0FBQztBQUVyQixhQUFhO0FBQ2IsTUFBTUMsYUFBYTtJQUFDTCxvREFBS0E7Q0FBQztBQUUxQixpQkFBaUI7QUFDVixNQUFNTSxRQUFRUixrREFBV0EsQ0FDOUJLLGlEQUFXQSxFQUNYQyxhQUNBSCw2RUFBbUJBLENBQUNGLHNEQUFlQSxJQUFJTSxjQUN2QztBQUVGLGtDQUFrQztBQUNsQyxNQUFNRSxZQUFZLElBQU1EO0FBRWpCLE1BQU1FLFVBQVVOLGlFQUFhQSxDQUFDSyxXQUFXIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vc3RvcmUvc3RvcmUuanM/MzY2MyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBjcmVhdGVTdG9yZSwgYXBwbHlNaWRkbGV3YXJlIH0gZnJvbSBcInJlZHV4XCI7XG5pbXBvcnQgdGh1bmsgZnJvbSBcInJlZHV4LXRodW5rXCI7XG5pbXBvcnQgeyBjb21wb3NlV2l0aERldlRvb2xzIH0gZnJvbSBcInJlZHV4LWRldnRvb2xzLWV4dGVuc2lvblwiO1xuaW1wb3J0IHsgY3JlYXRlV3JhcHBlciB9IGZyb20gXCJuZXh0LXJlZHV4LXdyYXBwZXJcIjtcbmltcG9ydCByb290UmVkdWNlciBmcm9tIFwiLi9yZWR1Y2Vyc1wiO1xuXG4vLyBpbml0aWFsIHN0YXRlcyBoZXJlXG5jb25zdCBpbml0YWxTdGF0ZSA9IHt9O1xuXG4vLyBtaWRkbGV3YXJlXG5jb25zdCBtaWRkbGV3YXJlID0gW3RodW5rXTtcblxuLy8gY3JlYXRpbmcgc3RvcmVcbmV4cG9ydCBjb25zdCBzdG9yZSA9IGNyZWF0ZVN0b3JlKFxuICByb290UmVkdWNlcixcbiAgaW5pdGFsU3RhdGUsXG4gIGNvbXBvc2VXaXRoRGV2VG9vbHMoYXBwbHlNaWRkbGV3YXJlKC4uLm1pZGRsZXdhcmUpKVxuKTtcblxuLy8gYXNzaWduaW5nIHN0b3JlIHRvIG5leHQgd3JhcHBlclxuY29uc3QgbWFrZVN0b3JlID0gKCkgPT4gc3RvcmU7XG5cbmV4cG9ydCBjb25zdCB3cmFwcGVyID0gY3JlYXRlV3JhcHBlcihtYWtlU3RvcmUpO1xuIl0sIm5hbWVzIjpbImNyZWF0ZVN0b3JlIiwiYXBwbHlNaWRkbGV3YXJlIiwidGh1bmsiLCJjb21wb3NlV2l0aERldlRvb2xzIiwiY3JlYXRlV3JhcHBlciIsInJvb3RSZWR1Y2VyIiwiaW5pdGFsU3RhdGUiLCJtaWRkbGV3YXJlIiwic3RvcmUiLCJtYWtlU3RvcmUiLCJ3cmFwcGVyIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./store/store.js\n");

/***/ }),

/***/ "next-redux-wrapper":
/*!*************************************!*\
  !*** external "next-redux-wrapper" ***!
  \*************************************/
/***/ ((module) => {

module.exports = require("next-redux-wrapper");

/***/ }),

/***/ "react-redux":
/*!******************************!*\
  !*** external "react-redux" ***!
  \******************************/
/***/ ((module) => {

module.exports = require("react-redux");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

module.exports = require("react/jsx-dev-runtime");

/***/ }),

/***/ "redux":
/*!************************!*\
  !*** external "redux" ***!
  \************************/
/***/ ((module) => {

module.exports = require("redux");

/***/ }),

/***/ "redux-devtools-extension":
/*!*******************************************!*\
  !*** external "redux-devtools-extension" ***!
  \*******************************************/
/***/ ((module) => {

module.exports = require("redux-devtools-extension");

/***/ }),

/***/ "redux-thunk":
/*!******************************!*\
  !*** external "redux-thunk" ***!
  \******************************/
/***/ ((module) => {

module.exports = require("redux-thunk");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/_app.js"));
module.exports = __webpack_exports__;

})();