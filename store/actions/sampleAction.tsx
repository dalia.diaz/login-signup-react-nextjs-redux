export const addUser = (data) => async (dispatch) => {
  dispatch({
    type: "ADD_USER",
    payload: {
      users: data,
    },
  });
};
