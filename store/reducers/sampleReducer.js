
const initialState = {
  sample: [],
  loading: true,
  users: [
    {
      email: "admin@admin.com",
      id: "$2b$10$rVwJGVobfIUHRI4rP7zN0OmT/fUJoSbUYR8acdtsrcbWTheXWTi5i",
      lastName: "admin",
      name: "admin",
      password: "$2b$10$iB.YY0oJywHrpRqonTkrBOp3ISgRsur5rhB0a66v42LffAkkL7c.6",
    },
  ],
};

const sampleReducer = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_USER":
      console.log("ADD_USER", action.payload, state.users);
      const updatedData = state.users.concat(action.payload.users);

      return {
        ...state,
        loading: false,
        users: updatedData,
      };

    default:
      return state;
  }
};

export default sampleReducer;
